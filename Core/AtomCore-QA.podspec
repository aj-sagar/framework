#
#  Be sure to run `pod spec lint AtomCore.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  s.name          = "AtomCore-QA"
  s.version       = "1.2.0"
  s.summary       = "Framework contain all the public models used by AtomSDK and AtomBPC."
  s.swift_versions = ['4.2', '5.0']


  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  s.description  = "Universal Framework contain all the public models used by AtomSDK and AtomBPC."

  s.homepage     = "https://github.com/AtomSDK/"


  s.license      = { :type => 'Copyright', :file => 'LICENSE' }

  s.author             = { "Atom By Secure" => "developer@atomapi.com" }

  s.platforms = {:ios => '10.0', :osx => '10.10'}

  s.source = { :git => "https://bitbucket.org/aj-sagar/framework.git", :tag => '1.0'}

  s.ios.vendored_frameworks = 'Core/AtomCore-QA/AtomCore-iOS/AtomCore.framework'
  s.osx.vendored_frameworks = 'Core/AtomCore-QA/AtomCore-OSX/AtomCore.framework'

end
