//
//  AtomCore_OSX.h
//  AtomCore-OSX
//
//  Created by Atom on 02/06/2019.
//  Copyright © 2019 Atom By Secure. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for AtomCore_OSX.
FOUNDATION_EXPORT double AtomCore_OSXVersionNumber;

//! Project version string for AtomCore_OSX.
FOUNDATION_EXPORT const unsigned char AtomCore_OSXVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AtomCore_OSX/PublicHeader.h>

#import <AtomCore/AtomCorePingData.h>
