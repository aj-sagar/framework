//
//  AtomCore.h
//  AtomCore
//
//  Created by Atom on 20/05/2019.
//  Copyright © 2019 Atom By Secure. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for AtomCore.
FOUNDATION_EXPORT double AtomCoreVersionNumber;

//! Project version string for AtomCore.
FOUNDATION_EXPORT const unsigned char AtomCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AtomCore/PublicHeader.h>
#import <AtomCore/AtomCorePingData.h>
