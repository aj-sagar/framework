#
#  Be sure to run `pod spec lint AtomSDKBySecure.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  spec.name         = "AtomSDK-QA"
  spec.version      = "3.0.0"
  spec.summary      = "AtomSDK by secure.com."

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  spec.description  = <<-DESC
  Using this SDK developer can establish VPN connection.
                   DESC

  spec.homepage     = "https://github.com/atomsdk/atomsdk-demo-ios"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Licensing your code is important. See https://choosealicense.com for more info.
  #  CocoaPods will detect a license file if there is a named LICENSE*
  #  Popular ones are 'MIT', 'BSD' and 'Apache License, Version 2.0'.
  #

  spec.license      = { :type => 'Copyright', :file => 'LICENSE' }


  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #

  spec.author             = { "Atom By Secure" => "developer@atomapi.com" }

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If this Pod runs only on iOS or OS X, then specify the platform and
  #  the deployment target. You can optionally include the target after the platform.
  #

   spec.platforms = {:ios => '10.0', :osx => '10.12'}



  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.
  #  Supports git, hg, bzr, svn and HTTP.
  #

   spec.source = { :git => "https://bitbucket.org/aj-sagar/framework.git", :tag => '1.0'}

  spec.ios.vendored_frameworks = 'SDK/AtomSDK-QA/AtomSDK-iOS/AtomSDK.framework'
  spec.osx.vendored_frameworks = 'SDK/AtomSDK-QA/AtomSDK-OSX/AtomSDK.framework'

  spec.dependency "AtomCore-QA"

  spec.ios.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '"$(PODS_ROOT)/AtomCore-QA/AtomCore-iOS/"' }
  spec.osx.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '"$(PODS_ROOT)/AtomCore-QA/AtomCore-OSX/"' }
end
