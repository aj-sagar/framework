// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.1 (swiftlang-1100.0.270.13 clang-1100.0.33.7)
// swift-module-flags: -target x86_64-apple-macos10.12 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Onone -module-name AtomSDK
import AtomCore
@_exported import AtomSDK
import Foundation
import Swift
import SystemConfiguration
@objc public class Authentication : ObjectiveC.NSObject, AtomCore.ManagerProtocol {
  @objc public var accessToken: AtomCore.AccessTokenModel?
  @objc public var activeBaseUrl: Swift.String?
  @objc public var apiUrl: AtomCore.ApiBaseUrlModel?
  @objc public var atomConfiguration: AtomCore.AtomConfiguration?
  @objc public class func setup(atomConfiguration: AtomCore.AtomConfiguration?, baseUrlModel: AtomCore.ApiBaseUrlModel, activeURL: Swift.String?)
  @objc public func generateAccessToken(authToken: @escaping (AtomCore.AccessTokenModel?, Foundation.NSError?) -> Swift.Void)
  @objc override dynamic public init()
  @objc deinit
}
public enum ReachabilityError : Swift.Error {
  case failedToCreateWithAddress(Darwin.sockaddr, Swift.Int32)
  case failedToCreateWithHostname(Swift.String, Swift.Int32)
  case unableToSetCallback(Swift.Int32)
  case unableToSetDispatchQueue(Swift.Int32)
  case unableToGetFlags(Swift.Int32)
}
@available(*, unavailable, renamed: "Notification.Name.reachabilityChanged")
public var ReachabilityChangedNotification: Foundation.NSNotification.Name
extension NSNotification.Name {
  public static var reachabilityChanged: Foundation.Notification.Name
}
public class Reachability {
  public typealias NetworkReachable = (AtomSDK.Reachability) -> ()
  public typealias NetworkUnreachable = (AtomSDK.Reachability) -> ()
  @available(*, unavailable, renamed: "Connection")
  public enum NetworkStatus : Swift.CustomStringConvertible {
    case notReachable
    case reachableViaWiFi
    case reachableViaWWAN
    public var description: Swift.String {
      get
    }
    public static func == (a: AtomSDK.Reachability.NetworkStatus, b: AtomSDK.Reachability.NetworkStatus) -> Swift.Bool
    public var hashValue: Swift.Int {
      get
    }
    public func hash(into hasher: inout Swift.Hasher)
  }
  public enum Connection : Swift.CustomStringConvertible {
    @available(*, deprecated, renamed: "unavailable")
    case none
    case unavailable
    case wifi
    case cellular
    public var description: Swift.String {
      get
    }
    public static func == (a: AtomSDK.Reachability.Connection, b: AtomSDK.Reachability.Connection) -> Swift.Bool
    public var hashValue: Swift.Int {
      get
    }
    public func hash(into hasher: inout Swift.Hasher)
  }
  public var whenReachable: AtomSDK.Reachability.NetworkReachable?
  public var whenUnreachable: AtomSDK.Reachability.NetworkUnreachable?
  @available(*, deprecated, renamed: "allowsCellularConnection")
  final public let reachableOnWWAN: Swift.Bool
  public var allowsCellularConnection: Swift.Bool
  public var notificationCenter: Foundation.NotificationCenter
  @available(*, deprecated, renamed: "connection.description")
  public var currentReachabilityString: Swift.String {
    get
  }
  @available(*, unavailable, renamed: "connection")
  public var currentReachabilityStatus: AtomSDK.Reachability.Connection {
    get
  }
  public var connection: AtomSDK.Reachability.Connection {
    get
  }
  required public init(reachabilityRef: SystemConfiguration.SCNetworkReachability, queueQoS: Dispatch.DispatchQoS = .default, targetQueue: Dispatch.DispatchQueue? = nil, notificationQueue: Dispatch.DispatchQueue? = .main)
  public convenience init(hostname: Swift.String, queueQoS: Dispatch.DispatchQoS = .default, targetQueue: Dispatch.DispatchQueue? = nil, notificationQueue: Dispatch.DispatchQueue? = .main) throws
  public convenience init(queueQoS: Dispatch.DispatchQoS = .default, targetQueue: Dispatch.DispatchQueue? = nil, notificationQueue: Dispatch.DispatchQueue? = .main) throws
  @objc deinit
}
extension Reachability {
  public func startNotifier() throws
  public func stopNotifier()
  @available(*, deprecated, message: "Please use `connection != .none`")
  public var isReachable: Swift.Bool {
    get
  }
  @available(*, deprecated, message: "Please use `connection == .cellular`")
  public var isReachableViaWWAN: Swift.Bool {
    get
  }
  @available(*, deprecated, message: "Please use `connection == .wifi`")
  public var isReachableViaWiFi: Swift.Bool {
    get
  }
  public var description: Swift.String {
    get
  }
}
