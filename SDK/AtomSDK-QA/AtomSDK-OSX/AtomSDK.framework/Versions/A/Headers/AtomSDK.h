//
//  AtomSDK_macOS.h
//  AtomSDK macOS
//
//  Created by Gaditek on 07/01/2019.
//

#import <Cocoa/Cocoa.h>

//! Project version number for AtomSDK_macOS.
FOUNDATION_EXPORT double AtomSDKVersionNumber;

//! Project version string for AtomSDK_macOS.
FOUNDATION_EXPORT const unsigned char AtomSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AtomSDK_macOS/PublicHeader.h>

#import <AtomSDK/AtomManager.h>
#import <AtomSDK/AtomCredential.h>
#import <AtomSDK/AtomProperties.h>
#import <AtomSDK/AtomStatus.h>
