//
//  AtomSDK_iOS.h
//  AtomSDK iOS
//
//  Created by Gaditek on 07/01/2019.
//

#import <UIKit/UIKit.h>

//! Project version number for AtomSDK.
FOUNDATION_EXPORT double AtomSDKVersionNumber;

//! Project version string for AtomSDK.
FOUNDATION_EXPORT const unsigned char AtomSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AtomSDK/PublicHeader.h>

#import <AtomCore/AtomCore-Swift.h>
#import <AtomSDK/AtomManager.h>
#import <AtomSDK/AtomCredential.h>
#import <AtomSDK/AtomProperties.h>
#import <AtomSDK/AtomStatus.h>

